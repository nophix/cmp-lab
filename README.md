# CMP-LAB
## Setup
```bash
sh ./install_helm.sh
sh ./install_repos.sh
```

### Longhorn (storage)
```bash
helm install longhorn longhorn/longhorn --namespace longhorn-system --create-namespace
kubectl patch storageclass standard -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
```

**IMPORTANT: Make sure open-iscsi is installed on all nodes**
```bash
sudo apt-get update
sudo apt-get install -y open-iscsi
```

### MetalLB (loadbalancer)
```bash
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.12.1/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.12.1/manifests/metallb.yaml
kubectl apply -f ./deployments/metallb-prep.yml
```

### Traefik (ingress)
Before running set `apiKey` to your cloudflare API-key in `./deployments/traefik-prep.yml`
```bash
kubectl apply -f ./deployments/traefik-prep.yml
helm install traefik traefik/traefik --namespace=traefik --values=./values-files/traefik-values.yaml --create-namespace
```

### Nginx
```bash
kubectl apply -f ./deployments/nginx.yml
```
